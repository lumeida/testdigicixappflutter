import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:fluttertoast/fluttertoast.dart';



class Home extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _Home();
  }

}
class _Home  extends State<Home>{
List users  = [];
 @override
  void initState() {
    super.initState();
    this.userList();
  }
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text("Chat" , style:  TextStyle(color: Colors.white, fontSize: 25),),
        backgroundColor: Colors.green[900],
        centerTitle: true,
      ),
      drawer: Drawer(
        child: ListView(
          children: <Widget>[
            DrawerHeader(
              child: Icon(Icons.chat, size: 100, color: Colors.green[900] ),
            ),
            ListTile(
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) {
                  return Home();
                }));
              },
              title: Text("Liste des utilisateurs", style: TextStyle(fontSize: 20,color: Colors.green[900]),),
              leading: Icon(Icons.list , color: Colors.green[900], size: 25,),
            ),
            ListTile(
              title: Text("Mes messages", style: TextStyle(fontSize: 20,color: Colors.green[900]),),
              leading: Icon(Icons.sms , color: Colors.green[900], size: 25,),
            ),
            ListTile(
              title: Text("Déconnexion", style: TextStyle(fontSize: 20,color: Colors.green[900]),),
              leading: Icon(Icons.exit_to_app , color: Colors.green[900], size: 25,),
            )
          ],
        ),
      ),
      body: this.users.isEmpty ? Center(child: Text("Aucun Utilisateur inscrit"),) :  ListView.builder(
          itemCount: this.users == null ? 0 : this.users.length,
          itemBuilder: (BuildContext context, int i)
          {
            return Card(
              color: Colors.white,
              elevation: 10,
              child: ListTile(title: Text(this.users[i]["pseudo"]),),
            );

          }
      ) 
    );
  }

 Future userList()  async {
   String url = "http://10.0.2.2:3333/user_list";
   final response =  await http.get(url);
   if (response.statusCode == 200) {
     this.setState(() {
        this.users = json.decode(response.body);
     });
   }
 }


}
