import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:fluttertoast/fluttertoast.dart';
import 'SignUp.dart';

import 'Home.dart';
class Login extends StatefulWidget{
  @override
  State createState() => _Login();

}
class _Login extends State<Login> {
  TextEditingController pseudo = TextEditingController();
  TextEditingController email = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return  Scaffold(
        body: Container(
          padding: EdgeInsets.all(20.0),
          color: Colors.green[900],
          child: Form(
            child: ListView(
              children: <Widget>[
                Column(
                  children: <Widget>[
                    SizedBox(height: 50,),
                    Container(width: 200, child: Icon(Icons.chat, size: 100, color: Colors.white,),),
                    SizedBox(height: 50,),
                    TextFormField(
                      controller: this.pseudo,
                      cursorColor: Colors.white,
                      style: TextStyle(color: Colors.white, fontSize: 20),
                      decoration: InputDecoration(
                        contentPadding: const EdgeInsets.all(16.0),
                        prefixIcon: Container(
                            padding: const EdgeInsets.only(top: 16.0, bottom: 16.0),
                            margin: const EdgeInsets.only(right: 8.0),
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(30.0),
                                    bottomLeft: Radius.circular(30.0),
                                    topRight: Radius.circular(30.0),
                                    bottomRight: Radius.circular(10.0)
                                )
                            ),
                            child: Icon(Icons.person, color: Colors.green[900],)),
                        hintText: "Pseudo",
                        hintStyle: TextStyle(color: Colors.white54),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(30.0),
                            borderSide: BorderSide.none
                        ),
                        filled: true,
                        fillColor: Colors.white.withOpacity(0.1),
                      ),

                    ),
                    SizedBox(height: 30.0),

                    TextFormField(
                      controller: this.email,
                      cursorColor: Colors.white,
                      style: TextStyle(color: Colors.white, fontSize: 20),
                      decoration: InputDecoration(
                        contentPadding: const EdgeInsets.all(16.0),
                        prefixIcon: Container(
                            padding: const EdgeInsets.only(top: 16.0, bottom: 16.0),
                            margin: const EdgeInsets.only(right: 8.0),
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(30.0),
                                    bottomLeft: Radius.circular(30.0),
                                    topRight: Radius.circular(30.0),
                                    bottomRight: Radius.circular(10.0)
                                )
                            ),
                            child: Icon(Icons.lock, color: Colors.green[900],)),
                        hintText: "Mot de passe",
                        hintStyle: TextStyle(color: Colors.white54),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(30.0),
                            borderSide: BorderSide.none
                        ),
                        filled: true,
                        fillColor: Colors.white.withOpacity(0.1),
                      ),
                      obscureText: true,
                    ),
                    SizedBox(height: 30.0),
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: RaisedButton(
                            onPressed: (){
                              this.loginUser(this.pseudo.text, this.email.text);
                            },
                            padding: const EdgeInsets.all(20.0),

                            color: Colors.white,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(30.0)
                            ),
                            child: Text('Se connecter', style: TextStyle(color: Colors.green[900], fontSize: 20.0),),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: 100,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        FlatButton(
                          textColor: Colors.white70,
                          child: Text("Créer un compte".toUpperCase()),
                          onPressed: (){
                            Navigator.push(context, MaterialPageRoute(builder: (context) {
                              return SignUp();
                            }));
                          },
                        ),
                        Container(
                          color: Colors.white54,
                          width: 2.0,
                          height: 20.0,
                        ),
                        FlatButton(
                          textColor: Colors.white70,
                          child: Text("Mot de passe oublier ?".toUpperCase()),
                          onPressed: (){},
                        ),
                      ],),
                  ],
                ),
              ],
            ),
          ),
        ),
      );
  }

  void loginUser(String pseudo, String password) async{
    String url = "http://10.0.2.2:3333/connect_user";
    final response = await http.post(url, body: {"pseudo" : pseudo,  "password" : password} );
    Map map = json.decode(response.body);
    if(response.statusCode==200){
      if(map["message"] == "Connected"){
        Navigator.push(context, MaterialPageRoute(builder: (context) {
          return Home();
        }));
      }else{
        Fluttertoast.showToast(
            msg: map["message"] ,
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            timeInSecForIos: 5,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0
        );

      }

    }

  }


}